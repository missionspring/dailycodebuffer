package com.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.master.model.UserModel;

@Repository
public interface UserRepo extends JpaRepository<UserModel, Integer> {

	UserModel findByUsername(String username);

}
