package com.master.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@GetMapping("/")
	public String home()
	{
		return "This is Home page";
	}
	
	@GetMapping("/user")
	public String user()
	{
		return "This is user";
	}
	
	@GetMapping("/admin")
	public String admin()
	{
		return "This is admin";
	}
}
